let mapleader=","

set backspace=indent,eol,start
set encoding=utf-8
set hidden
set hlsearch
set nocompatible
set number
set splitbelow
set splitright
set termguicolors

if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'airblade/vim-gitgutter'
Plug 'alvan/vim-closetag'
Plug 'gruvbox-community/gruvbox'
Plug 'LnL7/vim-nix'
Plug 'nbouscal/vim-stylish-haskell'
Plug 'neovimhaskell/haskell-vim'
Plug 'purescript-contrib/purescript-vim'
Plug 'reasonml-editor/vim-reason-plus'
Plug 'scrooloose/nerdcommenter'
Plug 'vim-airline/vim-airline'
Plug 'Yggdroot/indentLine'
call plug#end()

syntax on
filetype on
filetype plugin indent on

set background=dark
let g:gruvbox_italic=1
colorscheme gruvbox

let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1

let g:haskell_indent_in = 0
let g:purescript_indent_in = 0

noremap <leader>l <C-w>l
noremap <leader>h <C-w>h
noremap <leader>j <C-w>j
noremap <leader>k <C-w>k

fun! StripTrailingWhitespace()
    if &ft =~ 'patch'
        return
    endif
    %s/\s\+$//e
endfun

if &term =~ '256color'
    set t_ut=
endif

au BufWritePre * call StripTrailingWhitespace()
