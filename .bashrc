# Add bin to PATH
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/Projects/FretLink/toolbox/scripts:$PATH"

# Set default editor
export EDITOR="/usr/bin/vim"

# Increase bash history
export HISTSIZE=10000

# Enable alacritty bash completion
if [ -f "$HOME/alacritty_completion" ]; then
    source "$HOME/alacritty_completion"
fi

# Enable git completion
if [ -f /usr/share/bash-completion/completions/git ]; then
    source /usr/share/bash-completion/completions/git
fi

if [ -f "$HOME/.git-prompt.sh" ]; then
    source "$HOME/.git-prompt.sh"
fi

# Source dotfiles
dotfiles=("bash_prompt" "aliases_perso" "aliases_fretlink")

for file in "${dotfiles[@]}"; do
    [ -r ~/."$file" ] && [ -f ~/."$file" ] && source ~/."$file";
done;

# Source NVM
export NVM_DIR="$HOME/.nvm"
if [ -s "$NVM_DIR/nvm.sh" ]; then
    source "$NVM_DIR/nvm.sh"
fi

# Enable direnv
eval "$(direnv hook bash)"
