#!/bin/sh

# Install package
sudo apt install \
  curl \
  git \
  htop \
  make \
  pass \
  pwgen \
  tmux \
  tree \
  unrar \
  vim \
  xbindkeys

# Rust related install
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Tmux plugin manager install
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
